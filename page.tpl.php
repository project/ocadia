<?php
// $Id $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
<head>
  <title><?php print $head_title; ?></title>
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body <?php print theme('onload_attributes'); ?>>
<div id="wrapper">

  <!-- Search Box -->
  <?php if ($search_box): ?>
    <?php print $search_box; ?>
  <?php endif; ?>

  <!-- Header -->
  <div id="header">
    <?php
      // prepare header
      $site_fields = array();
      if ($site_name):
        $site_fields[] = check_plain($site_name);
      endif;
      if ($site_slogan):
        $site_fields[] = check_plain($site_slogan);
      endif;
      $site_title = implode(' ', $site_fields);
    ?>

    <!-- Site Name -->
    <?php if ($site_name): ?>
      <h1 id="site-name"><a href="<?php print url(); ?>" title="<?php print $site_title; ?>"><?php print($site_name); ?></a></h1>
    <?php endif; ?>

    <!-- Site Slogan -->
    <?php if ($site_slogan): ?>
      <div class="site-slogan"><?php print($site_slogan); ?></div>
    <?php endif; ?>

    <!-- Primary Links -->
    <?php if (isset($primary_links)): ?>
      <div id="primary" class="menu<?php if ($site_slogan) { print ' withslogan'; } ?>">
        <?php print theme('menu_links', $primary_links); ?>
     </div>
     <?php endif; ?>

    <!-- Secondary Links -->
    <?php if (isset($secondary_links)): ?>
      <?php
        $secondary_class = '';
        if ($primary_links && $site_slogan):
          $secondary_class = 'withprimaryslogan';
        elseif ($primary_links):
          $secondary_class = 'withprimary';
        elseif ($site_slogan):
          $secondary_class = 'withslogan';
        endif;
      ?>
      <div id="secondary" class="<?php print $secondary_class; ?> menu">
        <?php print theme('menu_links', $secondary_links); ?>
      </div>
    <?php endif; ?>
  </div>


  <!-- Main content -->
  <div id="content">

    <!-- Breadcrumb -->
    <?php if (!empty($breadcrumb)): ?>
      <div id="breadcrumb"><?php print $breadcrumb ?></div>
    <?php endif; ?>

    <!-- Page title -->
    <?php if (!empty($title)): ?>
      <h2 class="content-title"><?php print $title ?></h2>
    <?php endif; ?>
    
    <!-- Navigation tabs -->
    <?php if (!empty($tabs)): ?>
      <?php print $tabs ?>
    <?php endif; ?>
         
    <!-- Mission statement -->
    <?php if (!empty($mission)): ?>
      <p id="mission"><?php print $mission; ?></p>
    <?php endif; ?>
         
    <!-- Help text -->
    <?php if (!empty($help)): ?>
      <p id="help"><?php print $help; ?></p>
    <?php endif; ?>
         
    <!-- Messages -->
    <?php if (!empty($messages)): ?>
      <div id="message"><?php print $messages; ?></div>
    <?php endif; ?>
    
    
    <!-- Actual page content -->
    <?php print($content); ?>
  
  </div>

  
  <!-- Sidebar -->
  <div id="sidebar">
    <?php if ($left): ?>
      <div id="sidebar-left"><?php print $left; ?></div>
    <?php endif; ?>
    <?php if ($right): ?>
      <div id="sidebar-right"><?php print $right; ?></div>
    <?php endif; ?>
  </div>
  
  <!-- Footer -->
  <div id="footer">
    <p>
    Design By: <a href="http://beccary.com" title="Theme designed by Beccary">Beccary</a>
    and: <a href="http://scarto.abshost.net" title="Modified for Drupal by Scar_T">Scar_T</a>
    and <a href="http://www.stellapowerdesign.net" title="Currently maintained by Stella Power">snpower</a>
    <?php if ($footer_message): ?>
      - 
      <?php print $footer_message; ?>
    <?php endif; ?>
    </p>
    <?php if ($footer): ?>
      <?php print $footer; ?>
    <?php endif; ?>
  </div>

</div>

<?php print $closure; ?>

</body>
</html>
